# etl_finalized_values

## Steps

This repository contains code dedicated to finalizing the values created by the [residential](https://gitlab.com/ccao-data-science---modeling/models/ccao_res_avm) and [condominium](https://gitlab.com/ccao-data-science---modeling/models/ccao_condo_avm) automated valuation models (AVMs). Finalization steps include:

1. Fetch the full universe of PINs that need values. This is usually the BoR PINs from the year prior to the assessment year. This will include vacant land and other property types that don't need to be valued by AVMs.
2. Fetch sales (SQL), land rates (file), and initial model values (file) so we can append them to the universe of PINs.
3. Join all data sets, then horizontally merge the final values, keeping the newest value (from the model) where it exists.
4. Collapse values for multi-code properties into a single PIN + apply any proration factor.
5. Value land for all properties. For modeled values, disaggregate the total FMV into land and building FMV. For land alone, update land value using fixed land unit prices.

## Outputs

1. Excel workbook of values for desk review
2. DTBL_MODELVALS SQL table for internal use
3. Tyler upload format
