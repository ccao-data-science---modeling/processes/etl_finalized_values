SELECT 
	H.PIN AS meta_pin, 
	H.TAX_YEAR AS meta_year,
  H.HD_CLASS AS meta_class,
	T.DT_MLT_CD AS meta_multi_code,
	CHARS.MULTI_IND AS ind_multi_code,
	T.DT_PER_ASS AS meta_per_ass,
	T.DT_KEY_PIN AS meta_key_pin,
	T.DT_CDU AS meta_cdu,
	LEFT(H.HD_TOWN, 2) AS meta_town_code,
	H.HD_NBHD AS meta_nbhd,
	H.HD_ASS_LND * 10 AS prev_land_fmv,
	H.HD_ASS_BLD * 10 AS prev_bldg_fmv,
	H.HD_HD_SF AS char_hd_sf,
	CHARS.BLDG_SF AS char_bldg_sf,
	CHARS.TYPE_RESD AS char_type_resd,
	CHARS.AGE AS char_age
FROM AS_HEADTB H
LEFT JOIN CCAOSFCHARS CHARS
ON H.PIN = CHARS.PIN AND H.TAX_YEAR = CHARS.TAX_YEAR AND H.HD_CLASS = CHARS.CLASS
LEFT JOIN AS_DETAILT T
ON H.PIN = T.PIN AND H.TAX_YEAR = T.TAX_YEAR AND H.HD_CLASS = T.DT_CLASS
WHERE H.TAX_YEAR = {comparison_year_new}
AND (HD_CLASS BETWEEN 200 AND 298)
UNION
SELECT 
	H.PIN AS meta_pin, 
	H.TAX_YEAR AS meta_year,
  H.HD_CLASS AS meta_class,
	T.DT_MLT_CD AS meta_multi_code,
	NULL AS ind_multi_code,
	T.DT_PER_ASS AS meta_per_ass,
	T.DT_KEY_PIN AS meta_key_pin,
	T.DT_CDU AS meta_cdu,
	LEFT(H.HD_TOWN, 2) AS meta_town_code,
	H.HD_NBHD AS meta_nbhd,
	H.HD_ASS_LND * 10 AS prev_land_fmv,
	H.HD_ASS_BLD * 10 AS prev_bldg_fmv,
	H.HD_HD_SF AS char_hd_sf,
	NULL AS char_bldg_sf,
	NULL AS char_type_resd,
	T.DT_AGE AS char_age
FROM AS_HEADTB H
LEFT JOIN AS_DETAILT T
ON H.PIN = T.PIN AND H.TAX_YEAR = T.TAX_YEAR AND H.HD_CLASS = T.DT_CLASS
WHERE H.TAX_YEAR = {comparison_year_new}
AND (HD_CLASS IN (299, 399))