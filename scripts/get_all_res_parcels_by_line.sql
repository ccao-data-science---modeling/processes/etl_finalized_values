SELECT 
	T.PIN AS meta_pin, 
	T.TAX_YEAR AS meta_year,
  T.DT_CLASS AS meta_class,
	T.DT_MLT_CD AS meta_multi_code,
	T.DT_PER_ASS AS meta_per_ass,
	T.DT_KEY_PIN AS meta_key_pin,
	T.DT_CDU AS meta_cdu,
	LEFT(H.HD_TOWN, 2) AS meta_town_code,
	H.HD_NBHD AS meta_nbhd,
	T.DT_ASS_VAL_DET * 10 AS prev_year_fmv,
	T.DT_AGE AS char_age,
	T.DT_FT_FT_AR AS char_hd_sf
FROM AS_DETAILTB T
LEFT JOIN AS_HEADTB H
ON H.PIN = T.PIN AND H.TAX_YEAR = T.TAX_YEAR
WHERE T.TAX_YEAR = {comparison_year_new}
AND ((T.DT_CLASS BETWEEN 200 AND 299 OR T.DT_CLASS = 399) AND T.DT_CLASS != 288)