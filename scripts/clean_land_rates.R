library(tidyverse)
library(here)

# Load land rates from raw file
land_rates <- read_csv(here("input", "land_rates_raw.csv"))

# Get list of all 200-level residential classes
all_classes <- ccao::class_dict %>%
  filter(major_class_code == "2" | class_code == "399") %>%
  pull(class_code)

# Set aside townhome classes
th_classes <- c("210", "295")

# List all res classes minus townhomes
all_classes_but_th <- all_classes[!all_classes %in% th_classes]

# Convert land rates data into "tidy" style dataset 
land_rates_clean <- land_rates %>%
  mutate(
    class = case_when(
      class == "All" ~ list(all_classes),
      class == "2-10s/2-95s" ~ list(th_classes),
      class == "all other regression classes" ~ list(all_classes_but_th)
    )
  ) %>%
  unnest(class) %>%
  mutate(across(contains("unit_price"), parse_number)) %>%
  mutate(nbhd = paste0(
    str_pad(township_code, 2, "left", "0"),
    str_pad(nbhd, 3, "left", "0")
  )) %>%
  pivot_longer(
    contains("unit_price"),
    names_to = "year",
    values_to = "land_unit_price"
  ) %>%
  mutate(year = parse_number(year))

# Write cleaned rates to file
land_rates_clean %>%
  write_csv(here("input", "land_rates_clean.csv"), na = "")
